﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV2
{
    class Die
    {
        private int numberOfSides;
        //task no. 1:
        //private Random randomGenerator;
        //task no. 3:
        private RandomGenerator randomGenerator;

        //task no. 1:
        //public Die(int numberOfSides)
        //{
        //    this.numberOfSides = numberOfSides;
        //    this.randomGenerator = new Random();
        //}

        //task no. 2:
        //public Die(int numberOfSides, Random myRandomGenerator)
        //{
        //    this.numberOfSides = numberOfSides;
        //    this.randomGenerator = myRandomGenerator;
        //}

        //task no. 3:
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = RandomGenerator.GetInstance();
        }

        public int Roll()
        {
            //task no. 1 and 2:
            //int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
            //return rolledNumber;

            //task no. 3:
            int rolledNumber = this.randomGenerator.NextInt(1, numberOfSides + 1);
            return rolledNumber;
        }

        //task no. 7:
        public int GetNumberOfSides
        {
            get { return this.numberOfSides; }
        }
    }
}
