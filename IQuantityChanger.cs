﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV2
{
    //task no. 6:
    interface IQuantityChanger
    {
        void InsertDie(Die die);
        void RemoveAllDice();
    }
}
