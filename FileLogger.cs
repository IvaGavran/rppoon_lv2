﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV2
{
    class FileLogger:ILogger
    {
        private string filePath;

        public FileLogger(string filePath)
        {
            this.filePath = filePath;
        }

        //task no. 4:
        //public void log(string message)
        //{
        //    using (system.io.streamwriter filewriter = new system.io.streamwriter(this.filepath, true))
        //    {
        //        filewriter.writeline(message);
        //    }
        //}


        //task no. 5:
        public void Log(ILogable data)
        {
            using (System.IO.StreamWriter fileWriter = new System.IO.StreamWriter(this.filePath, true))
            {
                fileWriter.WriteLine(data.GetStringRepresentation());
            }
        }
    }
}
