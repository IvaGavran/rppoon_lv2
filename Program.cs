﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV2
{
    class Program
    {
        static void Main(string[] args)
        {
            DiceRoller myDiceRoller = new DiceRoller();
            int numberOfDice = 20;
            Random myRandomGenerator = new Random();

            while (myDiceRoller.DiceCount < numberOfDice)
            {
                //task no. 1:
                //Die randomDie = new Die(6);
                //myDiceRoller.InsertDie(randomDie);

                //task no. 2:
                //Die randomDie = new Die(6, myRandomGenerator);

                //task no. 3:
                Die randomDie = new Die(6);
                myDiceRoller.InsertDie(randomDie);
            }

            myDiceRoller.RollAllDice();

            IList<int> results = myDiceRoller.GetRollingResults();

            foreach (int result in results)
            {
                Console.WriteLine(result);
            }
        }
    }
}
